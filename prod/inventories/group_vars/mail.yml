---

# path to mail tls certificates
mail_cert_dir: "/etc/letsencrypt/live/{{ mail_domain }}"
mail_cert_public: "{{ mail_cert_dir }}/cert.pem"
mail_cert_private: "{{ mail_cert_dir }}/privkey.pem"

################################### certbot ###################################
# directory were certificates is saved, installed from, or revoked
certbot_certs_dir: "{{ mail_cert_dir }}"

# path to config files templates
certbot_ini_templates: "prod/templates/certbot"

# mail used for registration and recovery contact
certbot_email: "{{ mail_users.0.user }}@{{ mail_domain }}"

# list of domains to obtain a certificate for
certbot_domains: 
  - "{{ mail_domain }}"

# if the requested certificate matches an existing certificate, always keep
# the existing one until it is due for renewal (default: ask)
certbot_keep_until_expiring: true

# if a certificate already exists for the requested certificate name but does not
# match the requested domains, renew it now (default: false)
certbot_renew_with_new_domains: true

# type of generated private key: "rsa" or"ecdsa" (default: ecdsa)
certbot_key_type: "rsa"

# path to where certificate is saved, installed from, or revoked (default: None)
certbot_cert_path: "{{ mail_cert_public }}"

# path to where certificate is saved, installed from, or revoked (default: None)
certbot_key_path: "{{ mail_cert_private }}"

# test "renew" or "certonly" without saving any certificates to disk
certbot_dry_run: false

# set "true" to add cron job to update certificate
certbot_set_crone: true

################################### opendkim ##################################
# opendkim mail domain name
opendkim_domain_name: "{{ mail_domain }}"

# directory for dkim keys at ansible host 
opendkim_keys_local: "prod/dkimkeys/{{ mail_domain }}"

# directory contains opendkim config files templates
opendkim_templates_dir: "prod/templates/opendkim"

# path to opendkim.conf source
opendkim_conf_src: "prod/files/opendkim/opendkim.conf"

# path to opendkim.conf source
opendkim_file_src: "prod/files/opendkim/opendkim"

################################### postfix ###################################
# list of map supports to be installed 
# can take values: cdb, ldap, lmdb, mysql, pcre, pgsql, sqlite
postfix_map_support:
  - "pgsql"

# postfix user and group
postfix_group: { name: "postfix", gid: 1024 }
postfix_user: { name: "postfix", uid: 1024 }

# directory with postfix config files to be copied in to postfix directory
postfix_config_src: "prod/files/postfix"

# path to templates for postfix config files
postfix_templates_dir: "prod/templates/postfix"

# mail server hostname
postfix_hostname: "mail.{{ mail_domain }}"

# text that follows in the smtp server's greetings banner
postfix_smtpd_banner: "$myhostname ESMTP"

# announce tls support to remote smtp clients. default: no
postfix_smtpd_use_tls: "yes"

# tls protocols accepted by the postfix smtp server with tls encryption 
# if the list is empty, the server supports all available TLS protocol versions
# valid names are: "SSLv2", "SSLv3", "TLSv1", "TLSv1.1", "TLSv1.2" and "TLSv1.3"
postfix_smtpd_tls_protocols: ""

# enable sasl authentication
postfix_smtpd_sasl_auth_enable: "yes"

# information that the postfix passes through to the sasl plug-in implementation
postfix_smtpd_sasl_path: "private/auth"

# sasl plug-in type that the postfix should use for authentication
postfix_smtpd_sasl_type: "dovecot"

# sasl authentication security options that the postfix uses for tls encrypted smtp sessions
postfix_smtpd_sasl_tls_security_options: "noanonymous"

# file containing the optional postfix smtpd client tls session cache
postfix_smtpd_tls_cache_database: "btree:${data_directory}/smtpd_scache"

# optional lookup tables with aliases that apply only to local recipients
postfix_alias_maps: "hash:/etc/aliases proxy:pgsql:/etc/postfix/postfix-files.d/pgsql-aliases.cf"

# lookup tables with all names or addresses of local recipients
postfix_local_recipient_maps: "proxy:pgsql:/etc/postfix/postfix-files.d/pgsql-mailboxes.cf $alias_maps"

# message delivery transport that the local delivery agent should use for mailbox delivery to all local recipients
postfix_mailbox_transport: "lmtp:unix:private/dovecot-lmtp"

# protocols postfix will attempt to use when making or accepting connections
postfix_inet_protocols: "ipv4"

################################### dovecot ###################################
# list of supports to be installed 
# can take values: auth-lua, dev, gssapi, imapd, ldap, lmtpd, lucene, mysql, pgsql, pop3d, sieve, solr, sqlite
dovecot_map_support:
  - "lmtpd"
  - "pgsql"
  - "imapd"
  - "pop3d"

# dovecot user and group
dovecot_group: { name: "vmail", gid: 1025 }
dovecot_user: { name: "vmail", uid: 1025 }

# directory with dovecot config files to be copied in to dovecot directory
dovecot_config_src: ""

# path to templates for dovecot config files
dovecot_templates_dir: "prod/templates/dovecot"

# protocols we want to be serving: imap imaps pop3 pop3s
dovecot_protocols: "imap pop3 lmtp"

# list of ip or host addresses where to listen in for connections
dovecot_listen: "*"

# dictionary can be used by some plugins to store key=value lists
dovecot_dict: {}
#  - { key: "quota", value: "mysql:/etc/dovecot/dovecot-dict-sql.conf.ext" }
#  - { key: "expire", value: "sqlite:/etc/dovecot/dovecot-dict-sql.conf.ext" }

# userdb lookup retrieves post-login information specific to this user
dovecot_userdb: { driver: "prefetch", args: "" }

# passdb lookup information to authenticate the user
dovecot_passdb: { driver: "sql", args: "/etc/dovecot/dovecot-sql.conf" }

# ssl/tls support: yes, no, required
dovecot_ssl: "required"

# ssl ciphers to use
dovecot_ssl_cipher_list: "ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256"

# ssl protocols required
dovecot_ssl_protocols: "TLSv1.1 TLSv1.2"

# set to use ssl ciphers
dovecot_ssl_prefer_server_ciphers: "yes"

# format in which the password is stored
dovecot_default_pass_scheme: "SHA512"

# mailboxes directory
dovecot_maildir: "/home/mailboxes"

################################# spamassassin ################################
# spamassassin config files templates
spamassassin_file_template: "prod/templates/spamassassin/spamassassin.j2"
spamassassin_local_cf_template: "prod/templates/spamassassin/local.cf.j2"
spamassassin_99_filter_cf_template: "prod/templates/spamassassin/99_filter.cf.j2"

# list of spam keyphrases
spamassassin_bad_phrases: {}

################################ mail services ################################
# path to script template to add mailboxes
mail_add_mailboxes_template: "prod/templates/db/add_mailboxes.sh.j2"

# path to script to add mailboxes
mail_add_mailboxes_file: "/tmp/add_mailboxes.sh"
